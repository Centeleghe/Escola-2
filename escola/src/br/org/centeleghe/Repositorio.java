package br.org.centeleghe;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Repositorio {
    public void cadastrar(Pessoa pessoa){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        
        PreparedStatement preparedStatement = null;
        
        String insertTableSQL = "insert into pessoa"
                + "(id_pessoa, tipo_pessoa, nome_pessoa, idade_pessoa, endereco_pessoa, "
                + "semestre_aluno, curso_aluno, salario_funcionario, "
                + "disciplina_professor, setor_adm, funcao_adm) values "
                + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try{
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setInt(1, pessoa.getId());
            preparedStatement.setString(2, pessoa.getTipo());
            preparedStatement.setString(3, pessoa.getNome());
            preparedStatement.setInt(4, pessoa.getIdade());
            preparedStatement.setString(5, pessoa.getEndereco());
            
            if("aluno".equals(pessoa.getTipo())){
                Aluno aluno = (Aluno) pessoa;
                preparedStatement.setString(6, aluno.getSemestre());
                preparedStatement.setString(7, aluno.getCurso());
                preparedStatement.setNull(8, java.sql.Types.DOUBLE);
                preparedStatement.setString(9, null);
                preparedStatement.setString(10, null);
                preparedStatement.setString(11, null);
            } else if("professor".equals(pessoa.getTipo())){
                Professor professor = (Professor) pessoa;
                preparedStatement.setDouble(8, professor.getSalario());
                preparedStatement.setString(9, professor.getDisciplina());
                preparedStatement.setString(6, null);
                preparedStatement.setString(7, null);
                preparedStatement.setString(10, null);
                preparedStatement.setString(11, null);
                
            } else{
                FuncaoAdministrativa adm = (FuncaoAdministrativa) pessoa;
                preparedStatement.setDouble(8, adm.getSalario());
                preparedStatement.setString(10, adm.getSetor());
                preparedStatement.setString(11, adm.getFuncao());
                preparedStatement.setString(6, null);
                preparedStatement.setString(7, null);
                preparedStatement.setString(9, null);
            }
            preparedStatement.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
    }
    
    public ArrayList getAll(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        ArrayList<Pessoa> lista = new ArrayList<>();
        
        Statement statement;
        
        String selectSQL = "select * from pessoa";
        try{
            statement = dbConnection.prepareStatement(selectSQL);
            ResultSet rs = statement.executeQuery(selectSQL);
            
            while(rs.next()){
                String tipo = rs.getString("tipo_pessoa");
                if(tipo.equals("aluno")){
                    Aluno aluno = new Aluno();
                    aluno.setId(rs.getInt("id_pessoa"));
                    aluno.setTipo(rs.getString("tipo_pessoa"));
                    aluno.setNome(rs.getString("nome_pessoa"));
                    aluno.setIdade(rs.getInt("idade_pessoa"));
                    aluno.setEndereco(rs.getString("endereco_pessoa"));
                    aluno.setSemestre(rs.getString("semestre_aluno"));
                    aluno.setCurso(rs.getString("curso_aluno"));
                    lista.add(aluno);
                }else if(tipo.equals("professor")){
                    Professor professor = new Professor();
                    professor.setId(rs.getInt("id_pessoa"));
                    professor.setTipo(rs.getString("tipo_pessoa"));
                    professor.setNome(rs.getString("nome_pessoa"));
                    professor.setIdade(rs.getInt("idade_pessoa"));
                    professor.setEndereco(rs.getString("endereco_pessoa"));
                    professor.setSalario(rs.getDouble("salario_funcionario"));
                    professor.setDisciplina(rs.getString("disciplina_professor"));
                    lista.add(professor);
                }else{
                    FuncaoAdministrativa adm = new FuncaoAdministrativa();
                    adm.setId(rs.getInt("id_pessoa"));
                    adm.setTipo(rs.getString("tipo_pessoa"));
                    adm.setNome(rs.getString("nome_pessoa"));
                    adm.setIdade(rs.getInt("idade_pessoa"));
                    adm.setEndereco(rs.getString("endereco_pessoa"));
                    adm.setSalario(rs.getDouble("salario_funcionario"));
                    adm.setSetor(rs.getString("setor_adm"));
                    adm.setFuncao(rs.getString("funcao_adm"));
                    lista.add(adm);
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
        return lista;
    }
    
    public void update(Pessoa pessoa){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        
        String updateTableSQL = "update pessoa"
                + " set tipo_pessoa = ?, nome_pessoa = ?, idade_pessoa = ?, endereco_pessoa = ?, "
                + "semestre_aluno = ?, curso_aluno = ?, salario_funcionario = ?, "
                + "disciplina_professor = ?, setor_adm = ?, funcao_adm = ? where id_pessoa = ?";
        
        try{
            preparedStatement = dbConnection.prepareStatement(updateTableSQL);
            preparedStatement.setString(1, pessoa.getTipo());
            preparedStatement.setString(2, pessoa.getNome());
            preparedStatement.setInt(3, pessoa.getIdade());
            preparedStatement.setString(4, pessoa.getEndereco());
            
            if("aluno".equals(pessoa.getTipo())){
                Aluno aluno = (Aluno) pessoa;
                preparedStatement.setString(5, aluno.getSemestre());
                preparedStatement.setString(6, aluno.getCurso());
                preparedStatement.setNull(7, java.sql.Types.DOUBLE);
                preparedStatement.setString(8, null);
                preparedStatement.setString(9, null);
                preparedStatement.setString(10, null);
                preparedStatement.setInt(11, aluno.getId());
            } else if("professor".equals(pessoa.getTipo())){
                Professor professor = (Professor) pessoa;
                preparedStatement.setDouble(7, professor.getSalario());
                preparedStatement.setString(8, professor.getDisciplina());
                preparedStatement.setString(5, null);
                preparedStatement.setString(6, null);
                preparedStatement.setString(9, null);
                preparedStatement.setString(10, null);
                preparedStatement.setInt(11, professor.getId());
                
            } else{
                FuncaoAdministrativa adm = (FuncaoAdministrativa) pessoa;
                preparedStatement.setDouble(7, adm.getSalario());
                preparedStatement.setString(9, adm.getSetor());
                preparedStatement.setString(10, adm.getFuncao());
                preparedStatement.setString(5, null);
                preparedStatement.setString(6, null);
                preparedStatement.setString(8, null);
                preparedStatement.setInt(11, adm.getId());
            }
            preparedStatement.execute();
        }catch (SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
    }
    
    public void delete(Pessoa pessoa){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        
        String deleteSQL = "delete from Pessoa where id_pessoa = ?";
        
        try{
            preparedStatement = dbConnection.prepareStatement(deleteSQL);
            preparedStatement.setInt(1, pessoa.getId());
            preparedStatement.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
    }
}
